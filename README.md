# metmoa

metmoa pulls 10 random items from the Metropolitan Museum of Art's Public Domain pieces and display the title and artist of each item.

## Usage

```
> metmoa.py [-h] [-o output.json]
```

### Print to stdout

This command will output the titles of 10 random items to stdout.
```
> python src\metmoa.py
Terracotta fragments of a kylix (drinking cup) by Unknown Artist
...
```

### Write to a .json file
Optionally, include a .json file that you want metmoa to output to:
```
> python src\metmoa.py -o [filename].json
```
```json
// output.json

{
    "Terracotta fragments of a kylix (drinking cup)": {
        "accessionYear": "2011",
        "artistDisplayName": ""
    },
    ...
}
```