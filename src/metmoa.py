"""
This CLI prints to stdout 10 items that are now in the Public Domain
from the Metropolitan Museum of Art collection. The user can optionally
include a .json output file as an argument.
"""

import requests
import argparse
import json
import time
import random
from typing import Union, Dict


BASE_URL = "https://collectionapi.metmuseum.org/public/collection/v1/objects"
DatabaseEntry = Dict[str, Dict[str, str]]


def get_request(url: str) -> dict:
    """
    Given a URL produce the content of the response

    Args:
        url (str): a valid HTTP URL

    Returns:
        dict: a dictionary

    Example:

    .. code-block:: python

        > get_request("https://example.org")
        { "something" : {
            "another thing" : 2},
            ...
            }
        }


    """
    r = requests.get(url=url)
    return r.json()


def print_to_stdout(database: DatabaseEntry) -> None:
    """
    Print artwork title and artist name to stdout

    Args:
        database (DatabaseEntry): artwork data
    """
    for title, info in database.items():
        artist_name = info["artist_name"] or "Unknown Artist"
        print(f"{title} by {artist_name}")


def print_to_file(outfile: str, database: DatabaseEntry) -> None:
    """
    Print artwork json data to the outfile

    Args:
        outfile (str): name of the output file
        database (DatabaseEntry): artwork data
    """
    with open(outfile, "w") as f:
        json_str = json.dumps(database, indent=4)
        f.write(json_str)


def main(outfile: Union[str, bool]) -> None:
    json_dict = get_request(BASE_URL)
    total_objects = json_dict["total"]
    object_IDs = json_dict["objectIDs"]

    item_dict = {}
    for i in range(10):
        time.sleep(0.5)
        random_ID = random.randrange(0, total_objects - 1)
        current_obj = object_IDs[random_ID]
        json_obj = get_request(f"{BASE_URL}/{current_obj}")
        art_info = {}
        art_info["accession_year"] = json_obj["accessionYear"]
        art_info["artist_name"] = json_obj["artistDisplayName"]
        item_dict[json_obj["title"]] = art_info

    if outfile is None:
        print_to_stdout(item_dict)
    else:
        print_to_file(outfile, item_dict)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="metmoa",
        description="Prints 10 Public Domain items from the Metropolitan\
            Museum of Art Collection",
    )
    parser.add_argument(
        "-o",
        "--outfile",
        metavar="output_file",
        help="provide a file to print the output to",
    )
    args = parser.parse_args()
    outfile = args.outfile

    main(outfile)
